package specs.navinet;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import server.HRServer;

import com.mysql.jdbc.StringUtils;


@RunWith(ConcordionRunner.class)
public class AuthenticationFixture {
	
	static HttpURLConnection connection = null;
	static String sessionToken = null;
	static String sessionHeader = "X-session-token";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		server.HRServer.main(new String[] {});
	}
	
	@After
	public void tearDown() throws Exception {
		if(connection != null)
			connection.disconnect();
	}
	
	public Map<String,String> testLogin(String username, String password) throws IOException {
		Map<String, String> map = new HashMap<String, String>();
		
		doLogin(username, password);
		map.put("containsToken", readHeaders(connection).containsKey(sessionHeader)? "yes" : "no");
		map.put("code", String.valueOf(connection.getResponseCode()));
		
		return map;
	}
	
	public void doLogin(String username, String password) {
		connection = httpExecute("/login", "POST", "{'username':'" + username + "','password':'" + password + "'}");

		try {
			sessionToken = readHeaders(connection).get(sessionHeader).get(0);
		}
		catch (NullPointerException e){}
	}
	
	public void doLogout() {
		Map<String,String> map = new HashMap<String,String>();
		map.put(sessionHeader, sessionToken);
		connection = httpExecute("/logout", "POST", map);
	}
	
	public boolean testLogoutCode(String code) throws IOException {
		doLogout();
		return connection.getResponseCode() == Integer.parseInt(code);
	}
	
	public void invalidateSessionToken() {
		sessionToken = "wrong";
	}
	
	private HttpURLConnection httpExecute(String url, String method, Map<String,String> headers) {
		return httpExecute(url, method, "", headers);
	}
	
	private HttpURLConnection httpExecute(String url, String method, String body) {
		return httpExecute(url, method, body, null);
	}
	
	private HttpURLConnection httpExecute(String urlStr, String method, String body, Map<String,String> headers) {

		URL url;
	    HttpURLConnection connection = null;  
	    
	    String targetURL = "http://localhost:" + HRServer.port + urlStr;
	    
	    try {
	      //Create connection
	      url = new URL(targetURL);
	      connection = (HttpURLConnection)url.openConnection();
	      connection.setRequestMethod(method);
				
	      connection.setRequestProperty("Content-Length", "" + 
	               Integer.toString(body.getBytes().length));
	      
	      //add headers
	      if(headers != null) {
		      for(String key : headers.keySet()) {
		    	  connection.setRequestProperty(key, headers.get(key));
		      }
	      }
				
	      connection.setUseCaches (false);
	      connection.setDoInput(true);
	      connection.setDoOutput(true);

	      if(! StringUtils.isEmptyOrWhitespaceOnly(body)) {
		      //Send request
		      DataOutputStream wr = new DataOutputStream (
		                  connection.getOutputStream ());
		      wr.writeBytes (body);
		      wr.flush ();
		      wr.close ();
	      }

	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    
	    return connection;
	}
	
	
	@SuppressWarnings("unused")
	private String readBody(HttpURLConnection connection) throws IOException {
		InputStream is = connection.getInputStream();
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		String line;
		StringBuffer response = new StringBuffer(); 
		while((line = rd.readLine()) != null) {
			response.append(line);
			response.append('\r');
		}
		rd.close();
		return response.toString().toString();
	      
	}
	
	private Map<String, List<String>> readHeaders(HttpURLConnection connection) {
		return connection.getHeaderFields();
	}
	
}
