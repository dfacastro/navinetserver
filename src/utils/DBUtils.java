package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.StringUtils;

import sun.misc.IOUtils;

public class DBUtils {
	
	public static Connection connect = null;
	
	private DBUtils () {}
	
	/**
	 * Initiates DB connection
	 * @throws SQLException
	 */
	public static void setup() throws SQLException {
		connect = DriverManager
		          .getConnection("jdbc:mysql://localhost:4000/navinet?"
		                  + "user=root&password=diogo1989");
	}

	/**
	 * Create the database schema
	 * @throws IOException
	 * @throws SQLException
	 */
	public static void createSchema() throws IOException, SQLException {
		//read sql script
		String schema = readFile("sql/init.sql");
		
		//execute sql script
		executeMultipleStatements(schema);
	}
	
	/**
	 * Populate the DB with seed data
	 * @throws IOException
	 * @throws SQLException
	 */
	public static void seedDB() throws IOException, SQLException {
		//read sql script
		String seeds = readFile("sql/seeds.sql");
		
		//execute sql script
		executeMultipleStatements(seeds);
	}
	
	private static void executeMultipleStatements(String queries) throws SQLException {
		//turn on transactions
		connect.setAutoCommit(false);
		
		Statement s = connect.createStatement();
		String delimiter = ";";
		
		//split script into multiple statements and execute them
		for (String sql : queries.split(delimiter)) {
			if (! StringUtils.isEmptyOrWhitespaceOnly(sql)) {
				s.executeUpdate(sql);
			}
		}
		
		//commit and close
		s.close();
		connect.commit();
	}
	
	private static String readFile(String filepath) throws IOException {
		java.io.File file = new java.io.File(filepath);
		int size = (int) file.length();

		FileInputStream inputStream = new FileInputStream(filepath);
		String schema = "";
		
	    try {
	        byte[] b = IOUtils.readFully(inputStream, size, true);
	        schema = new String(b);	        
	    } finally {
	        inputStream.close();
	    }
	    
	    return schema;
	}
}
