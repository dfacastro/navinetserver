package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.json.JSONString;

import sun.net.www.protocol.http.HttpURLConnection;

import com.sun.net.httpserver.HttpExchange;

public class StreamUtils {
	
	private StreamUtils() {}

	/**
	 * Read String from InputStream
	 * @param in - the InputStream
	 * @return the String
	 * @throws IOException
	 */
	public static String read(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader r = new BufferedReader(new InputStreamReader(in), 1000);
		for (String line = r.readLine(); line != null; line = r.readLine()) {
			sb.append(line);
		}
		in.close();
		return sb.toString();
	}

	/**
	 * Writes a serializable object to a HTTP response.
	 * @param he - the HTTP object
	 * @param jsonstr - the serializable object
	 * @throws IOException
	 */
	public static void write(HttpExchange he, JSONString jsonstr) throws IOException {
		String str = jsonstr.toJSONString();
		OutputStream os = he.getResponseBody();
		
		he.sendResponseHeaders(HttpURLConnection.HTTP_OK, str.length());
		
		os.write(str.getBytes());
		os.close();
	}

}
