package utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerUtils {
	private LoggerUtils() {}
	
	public static void log(Level level, String msg) {
		Logger.getLogger("navinet").log(level, msg);
	}
}
