package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.sql.SQLException;
import java.util.logging.Level;

import utils.DBUtils;
import utils.LoggerUtils;

import com.sun.net.httpserver.HttpServer;

import controllers.AuthController;
import controllers.UsersController;

public class HRServer {
	
	public static int port = 4001;

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		try {
			
			
			DBUtils.setup();
			DBUtils.createSchema();
			//DBUtils.seedDB();
			
			HttpServer server = HttpServer.create(new InetSocketAddress(port), 8);
			
			AuthController authController = new AuthController();
			UsersController usersController = new UsersController();
			
			server.createContext("/login", authController.loginHandler);
			server.createContext("/logout", authController.logoutHandler);
			server.createContext("/users", usersController.usersHandler);
			
			
			server.start();
			
			
		} catch (IOException e) {
			LoggerUtils.log(Level.SEVERE, e.getMessage());
			System.err.println("Could not start server - exiting");
		} catch (SQLException e) {
			LoggerUtils.log(Level.SEVERE, e.getMessage());
			System.err.println("Could not establish connection to the database - exiting");
		}

	}

}
