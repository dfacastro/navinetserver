package models;

public interface CacheInterface<T> {
	
	public User find(T obj);
	public void insert(T key, User user);
	public void remove(T key);

}
