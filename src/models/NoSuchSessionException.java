package models;

public class NoSuchSessionException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8920054063621362962L;

	public NoSuchSessionException(String sessionToken) {
		super("No session found with token '" + sessionToken + "'.");
	}
}
