package models;

public class NoSuchUserException extends InvalidCredentialsException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -103131610802081957L;
	
	public NoSuchUserException(String username) {
		super("No account found with username '" + username + "'.");
	}
	
	public NoSuchUserException(int id) {
		super("No account found with id '" + id + "'.");
	}
	
}
