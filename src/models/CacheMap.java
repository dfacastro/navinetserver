package models;

import java.util.HashMap;
import java.util.Map;

public class CacheMap<T> implements CacheInterface<T> {
	
	Map<T, User> map = new HashMap<T, User>();

	@Override
	public User find(T obj) {
		return map.get(obj);
	}

	@Override
	public void insert(T key, User user) {
		map.put(key, user);
	}

	@Override
	public void remove(T key) {
		map.remove(key);
	}
	
	
}
