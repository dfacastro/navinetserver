package models;

import java.util.ArrayList;
import java.util.logging.Level;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.*;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONString;

import utils.DBUtils;
import utils.LoggerUtils;


public class User implements JSONString {
	
	public ArrayList<String> sessionTokens = new ArrayList<String>();
	public String username;
	public String name;
	public int id;
	private String hashedPassword;
	protected int incorrectAttempts;
	
	private User() {}
	
	protected User(String username) throws NoSuchUserException, SQLException {			
		PreparedStatement statement = DBUtils.connect
				.prepareStatement("select * from users u left join sessions s ON u.username = s.username WHERE u.username = ?");
		statement.setString(1, username);

		ResultSet res = statement.executeQuery();

		if (!res.next())
			throw new NoSuchUserException(username);

		parseUserQuery(res, this);
		statement.close();
	}
	
	protected User(int id) throws SQLException, NoSuchUserException {
		PreparedStatement statement = DBUtils.connect
				.prepareStatement("select * from users u left join sessions s ON u.username = s.username WHERE u.id = ?");
		statement.setInt(1, id);

		ResultSet res = statement.executeQuery();

		if (!res.next())
			throw new NoSuchUserException(id);

		parseUserQuery(res, this);
		statement.close();
	}
	
	/**
	 * Instantiate a User given a valid session token
	 * @param sessionToken - a valid session token
	 * @return the instantiated user
	 * @throws SQLException
	 * @throws NoSuchSessionException
	 */
	public static User instantiateFromSessionToken(String sessionToken) throws SQLException, NoSuchSessionException {
		
		String query = "SELECT *" + 
						" FROM users u, sessions s, (" +
						"SELECT u.username FROM users u, sessions s WHERE s.sessionToken = ? AND u.username = s.username" +
						") as userWithToken " +
						"WHERE u.username = s.username AND s.username = userWithToken.username";
		
		PreparedStatement statement = DBUtils.connect
				.prepareStatement(query);
		statement.setString(1, sessionToken);

		ResultSet res = statement.executeQuery();

		if (!res.next())
			throw new NoSuchSessionException(sessionToken);
		
		User user = new User();
		parseUserQuery(res, user);
		statement.close();
		return user;
		
	}
	
	/**
	 * Parse a query's results into user's fields
	 * @param res - the query's results
	 * @param user - the user whose fields are going to be written
	 * @throws SQLException
	 */
	private static void parseUserQuery(ResultSet res, User user) throws SQLException {
		user.username = res.getString("username");
		user.name = res.getString("name");
		user.hashedPassword = res.getString("password");
		user.id = res.getInt("id");
		user.incorrectAttempts = res.getInt("incorrectAttempts");

		do {
			user.sessionTokens.add(res.getString("sessionToken"));
		} while (res.next());
	}
	
	/**
	 * Creates and returns a sessionToken for this user.
	 * @param password - the password for this user.
	 * @return a newly created sessionToken.
	 * @throws SQLException
	 * @throws InvalidPasswordException 
	 * @throws UserLockedDownException 
	 */
	public String login(String password) throws SQLException, InvalidPasswordException, UserLockedDownException {
		
		if(isLockedDown()) {
			throw new UserLockedDownException();
		}
		
		String hashedPassword = hashPassword(password);
		
		//check if the given password is correct
		if(!hashedPassword.equals(this.hashedPassword)) {
			incIncorrectAttemps();
			throw new InvalidPasswordException(username);
		}
		resetIncorrectAttempts();
		
		//create a new session
		PreparedStatement statement = DBUtils.connect.prepareStatement("insert into sessions(username) values(?)", Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, username);
		statement.executeUpdate();
		
		ResultSet rs = statement.getGeneratedKeys();
		rs.next();
		int sessionID = rs.getInt(1);
		statement.close();
		
		//retrieve newly created sessionToken
		statement = DBUtils.connect.prepareStatement("SELECT sessionToken FROM sessions WHERE id = ?");
		statement.setInt(1, sessionID);
		
		rs = statement.executeQuery();
		rs.next();
		String sessionToken = rs.getString("sessionToken");
		statement.close();
		
		sessionTokens.add(sessionToken);
		
		return sessionToken;
	}
	
	private void incIncorrectAttemps() throws SQLException {
		// TODO Auto-generated method stub
		if(incorrectAttempts < 3) {
			setIncorrectAttempts(incorrectAttempts+1);
			
		}
	}
	
	private void resetIncorrectAttempts() throws SQLException {
		setIncorrectAttempts(0);
	}
	
	protected void setIncorrectAttempts(int counter) throws SQLException {
		incorrectAttempts = counter;
		PreparedStatement statement = null;
		try {
			statement = DBUtils.connect.prepareStatement("UPDATE users SET incorrectAttempts = ? WHERE username = ?");
			statement.setInt(1, incorrectAttempts);
			statement.setString(2, username);
			
			statement.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if(statement != null)
				statement.close();
		}
	}
	
	private boolean isLockedDown() {
		return incorrectAttempts ==3;
	}

	/**
	 * Deletes the given session token
	 * @param sessionToken
	 * @throws SQLException
	 */
	public void logout(String sessionToken) throws SQLException {
		
		//delete from DB
		PreparedStatement statement = DBUtils.connect.prepareStatement("DELETE FROM sessions WHERE sessionToken = ?");
		statement.setString(1, sessionToken);
		statement.executeUpdate();
		statement.close();
		
		//delete from memory
		sessionTokens.remove(sessionToken);
	}

	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		try {
			obj.put("username", username);
			obj.put("name", name);
			obj.put("id", id);
		} catch (JSONException e) {
			LoggerUtils.log(Level.INFO, e.getMessage());
		}
		
		
		return obj.toString();
	}
	
	/**
	 * Applies the MD5 algorithm to the given password
	 * @param password
	 * @return
	 */
	private String hashPassword(String password) {
		MessageDigest md5 = null;
		try {
			md5 = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {}		
		return new BigInteger(1, md5.digest(password.getBytes())).toString(16);
	}

}
