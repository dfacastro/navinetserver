package models;

public class InvalidPasswordException extends InvalidCredentialsException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8852604129201935507L;

	public InvalidPasswordException(String username) {
		super("Wrong password for user '" + username + "'.");
	}
}
