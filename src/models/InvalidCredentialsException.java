package models;

public abstract class InvalidCredentialsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2401844902589326989L;
	
	public InvalidCredentialsException(String msg) {
		super(msg);
	}
}
