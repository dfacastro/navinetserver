package models;

import java.sql.SQLException;


public class UsersManager {
	

	
	
	private CacheInterface<String> cacheUsernames = new CacheMap<String>();
	private CacheInterface<Integer> cacheIDs = new CacheMap<Integer>();
	private CacheInterface<String> cacheSessionTokens = new CacheMap<String>();

	private static UsersManager instance = new UsersManager();
	
	private UsersManager() {}
	
	public static UsersManager getInstance() {
		return instance;
	}
	
	public void setCache(CacheInterface c) {
		cacheUsernames = c;
		cacheIDs = c;
		cacheSessionTokens = c;
	}
	
	
	/**
	 * Find a user by its username
	 * @param username - the username of the user
	 * @return the user that's being searched for
	 * @throws NoSuchUserException
	 * @throws SQLException
	 */
	public User findByUsername(String username) throws NoSuchUserException, SQLException {
		
		User user = cacheUsernames.find(username);
		if(user != null)
			return user;
		
		user = new User(username);
		cacheUser(user);

		return user;
	}
	
	/**
	 * Find a user by its ID
	 * @param id - the ID of the user
	 * @return the user that's being searched for
	 * @throws NoSuchUserException
	 * @throws SQLException
	 */
	public User findByID(int id) throws NoSuchUserException, SQLException {
		User user = cacheIDs.find(id);
		if(user != null)
			return user;
		
		user = new User(id);
		cacheUser(user);
		return user;
	}
	
	/**
	 * Find a user by one of its valid session tokens
	 * @param sessionToken - a user's valid session token
	 * @return the user that's being searched for
	 * @throws SQLException
	 * @throws NoSuchSessionException
	 */
	public User findBySessionToken(String sessionToken) throws SQLException, NoSuchSessionException {
		
		User user = cacheSessionTokens.find(sessionToken);
		if(user != null)
			return user;
		
		user = User.instantiateFromSessionToken(sessionToken);
		cacheUser(user);
		return user;
	}
	
	private void cacheUser(User user) {
		cacheUsernames.insert(user.username, user);
		cacheIDs.insert(user.id, user);
		for(String token : user.sessionTokens)
			cacheSessionTokens.insert(token, user);
	}
	
}
