package controllers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;

import models.InvalidCredentialsException;
import models.NoSuchSessionException;
import models.User;
import models.UserLockedDownException;
import models.UsersManager;

import org.json.JSONException;
import org.json.JSONObject;

import sun.net.www.protocol.http.HttpURLConnection;
import utils.LoggerUtils;
import utils.StreamUtils;


import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;


public class AuthController {
	
	public HttpHandler logoutHandler = new HttpHandler() {

		@Override
		public void handle(HttpExchange he) throws IOException {
			if (he.getRequestMethod().toLowerCase().equals("post")) {
	        	System.out.println("POST " + he.getRequestURI());
	            logout(he);
	        } else {
	            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
	            he.getResponseBody().close();
	        }
		}
	};
	public HttpHandler loginHandler = new HttpHandler() {

		@Override
		public void handle(HttpExchange he) throws IOException {
			if (he.getRequestMethod().toLowerCase().equals("post")) {
	        	System.out.println("POST " + he.getRequestURI());
	            login(he);
	        } else {
	            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
	            he.getResponseBody().close();
	        }
		}
	};
	
	private void login(HttpExchange he) throws IOException {
		InputStream is = he.getRequestBody();
        JSONObject body;
        
        try {
			body = new JSONObject(StreamUtils.read(is));
			
			String username = body.getString("username");
			String password = body.getString("password");
			
			User user = UsersManager.getInstance().findByUsername(username);
			String sessionToken = user.login(password);
			
			Headers headers = he.getResponseHeaders();
			headers.add("X-Session-Token", sessionToken);
			
			he.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
			
		} catch (JSONException e) {
			LoggerUtils.log(Level.INFO, e.getMessage());
			he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
		} catch (InvalidCredentialsException e) {
			LoggerUtils.log(Level.INFO, e.getMessage());
			he.sendResponseHeaders(HttpURLConnection.HTTP_UNAUTHORIZED, 0);
		} catch (UserLockedDownException e) {
			LoggerUtils.log(Level.INFO, e.getMessage());
			he.sendResponseHeaders(HttpURLConnection.HTTP_FORBIDDEN, 0);
		} catch (Exception e) { //including SQL Exception
			LoggerUtils.log(Level.INFO, e.getMessage());
			he.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
		}
        
        OutputStream os = he.getResponseBody();
        os.close();
	}
	
	private void logout(HttpExchange he) throws IOException {
		
		Headers headers = he.getRequestHeaders();
		String sessionToken = headers.getFirst("X-Session-Token");
		
		if(sessionToken != null) {
			
			try {
				//find user and logout
				User user = UsersManager.getInstance().findBySessionToken(sessionToken);
				user.logout(sessionToken);
				
				he.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
			} catch (NoSuchSessionException e) {
				LoggerUtils.log(Level.INFO, e.getMessage());
				he.sendResponseHeaders(HttpURLConnection.HTTP_UNAUTHORIZED, 0);
			} catch (Exception e) { //including SQL Exception
				LoggerUtils.log(Level.INFO, e.getMessage());
				he.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
			}	
		} else {
			LoggerUtils.log(Level.INFO, "Received bad request");
			he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
		}
		
        OutputStream os = he.getResponseBody();
        os.close();
	}
	

}
