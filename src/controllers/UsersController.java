package controllers;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;
import java.util.logging.Level;

import models.NoSuchUserException;
import models.User;
import models.UsersManager;

import sun.net.www.protocol.http.HttpURLConnection;
import utils.LoggerUtils;
import utils.StreamUtils;
import utils.URIUtils;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public class UsersController {
	
	public HttpHandler usersHandler = new HttpHandler() {

		@Override
		public void handle(HttpExchange he) throws IOException {
			if (he.getRequestMethod().toLowerCase().equals("get")) {
	        	System.out.println("GET " + he.getRequestURI());
	        	showProfile(he);
	            
	        } else {
	            he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_METHOD, 0);
	            he.getResponseBody().close();
	        }
		}
	};

	private void showProfile(HttpExchange he) throws IOException {
		String query = he.getRequestURI().getQuery();
		Map<String, String> queryParams = URIUtils.getQueryMap(query);
		
		Headers headers = he.getRequestHeaders();
		String sessionToken = headers.getFirst("X-Session-Token");
		
		if(queryParams.containsKey("id") && sessionToken != null) {
			try {
				//validate token
				UsersManager.getInstance().findBySessionToken(sessionToken);
				
				//find requested user
				String userID = queryParams.get("id");
				User user = UsersManager.getInstance().findByID(Integer.parseInt(userID));
				
				//send serialized info
				StreamUtils.write(he, user);
		        return;
		        
			} catch (NumberFormatException e) {
				LoggerUtils.log(Level.INFO, e.getMessage());
				he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
			} catch (NoSuchUserException e) {
				LoggerUtils.log(Level.INFO, e.getMessage());
				he.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, 0);
			} catch (Exception e) { //including SQL Exception
				LoggerUtils.log(Level.INFO, e.getMessage());;
				he.sendResponseHeaders(HttpURLConnection.HTTP_INTERNAL_ERROR, 0);
			}
		} else {
			LoggerUtils.log(Level.INFO, "Received bad request");
			he.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
		}
		
        OutputStream os = he.getResponseBody();
        os.close();
	}
}
