package models;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import models.EmptyCache;
import models.UserLockedDownException;
import models.UsersManager;
import models.InvalidPasswordException;
import models.NoSuchSessionException;
import models.NoSuchUserException;
import models.User;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import utils.DBUtils;

public class UsersTestSuite {

	static String seedUsername = "diogo";
	static String seedName = "Diogo Castro";
	static String seedPassword = "1234";
	
	
	/**
	 * By declaring a subclass of User in the same package as the JUnit tests,
	 * these tests are able to access and test User's protected methods (its constructors).
	 * @author Diogo
	 *
	 */
	class UserTest extends User {

		protected UserTest(int id) throws SQLException, NoSuchUserException {
			super(id);
		}

		protected UserTest(String username) throws NoSuchUserException, SQLException {	
			super(username);
		}
	}
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DBUtils.setup();
		DBUtils.createSchema();		
	}

	@Before
	public void setUp() throws Exception {
		DBUtils.seedDB();
		
		//delete loaded users
	/*	Field field = UsersManager.class.getDeclaredField("users");
		field.setAccessible(true);
		field.set(UsersManager.getInstance(), new ArrayList<User>());
		*/
		
		UsersManager.getInstance().setCache(new EmptyCache());
	}

	@Test
	public void testUserString() throws NoSuchUserException, SQLException {
		User user = new UserTest(seedUsername);
		assertEquals("Username was not loaded correctly", user.username, seedUsername);
		assertEquals("Name was not loaded correctly", user.name, seedName);
		assertEquals("Session tokens were not loaded correctly", user.sessionTokens.size(), 3);
		
	}
	
	@Test
	public void testUserStringThrowsNoSuchUserException() throws NoSuchUserException, SQLException {
		exception.expect(NoSuchUserException.class);
		new UserTest("unknown");
	}

	@Test
	public void testUserInt() throws SQLException, NoSuchUserException {
		int userID = getUserID(seedUsername);
		User user = new UserTest(userID);
		assertEquals("Username was not loaded correctly", user.username, seedUsername);
		assertEquals("Name was not loaded correctly", user.name, seedName);
		assertEquals("Session tokens were not loaded correctly", user.sessionTokens.size(), 3);
	}
	
	@Test
	public void testUserIntThrowsNoSuchUserException() throws NoSuchUserException, SQLException {
		exception.expect(NoSuchUserException.class);
		new UserTest(-1);
	}

	@Test
	public void testInstantiateFromSessionToken() throws SQLException, NoSuchSessionException {
		String sessionToken = getSessionToken(seedUsername);
		
		User user = UserTest.instantiateFromSessionToken(sessionToken);
		assertEquals("Username was not loaded correctly", user.username, seedUsername);
		assertEquals("Name was not loaded correctly", user.name, seedName);
		assertEquals("Session tokens were not loaded correctly", user.sessionTokens.size(), 3);
	}
	
	@Test
	public void testInstantiateFromSessionTokenThrowsNoSuchSessionException() throws SQLException, NoSuchSessionException {
		exception.expect(NoSuchSessionException.class);
		User.instantiateFromSessionToken("token");
	}

	@Test
	public void testLogin() throws NoSuchUserException, SQLException, InvalidPasswordException, UserLockedDownException {
		User user = new UserTest(seedUsername);
		String sessionToken = user.login(seedPassword);
		assertNotNull("Session token was null", sessionToken);
		assertTrue("Session token was not kept as part of the user's sessionTokens collection",
				user.sessionTokens.contains(sessionToken));
		assertEquals("Session token had an unexpected length", sessionToken.length(), 36);
	}
	
	@Test
	public void testLoginThrowsInvalidPasswordException() throws InvalidPasswordException, NoSuchUserException, SQLException, UserLockedDownException {
		exception.expect(InvalidPasswordException.class);
		new UserTest(seedUsername).login("wrong-password");
	}
	
	@Test
	public void testLoginLockDown() throws NoSuchUserException, SQLException, UserLockedDownException, InvalidPasswordException {
		User user = new UserTest(seedUsername);
		
		for(int i = 0; i < 3; i++) {
			try {
				user.login("wrong");
			} catch (InvalidPasswordException e) {
			}
		}
		exception.expect(UserLockedDownException.class);
		user.login("wrong");
	}
	
	@Test
	public void testLockDownCounterReset() throws SQLException, UserLockedDownException, NoSuchUserException, InvalidPasswordException {
		User user = new User(seedUsername);
		
		user.setIncorrectAttempts(1);
		
		user.login(seedPassword);
		assertEquals(user.incorrectAttempts, 0);
		
	}
	

	@Test
	public void testLogout() throws NoSuchUserException, SQLException, InvalidPasswordException {
		User user = new UserTest(seedUsername);
		String token = "token";
		user.sessionTokens.add(token);
		user.logout(token);
		
		assertTrue("Session token was not erased from the user's sessionTokens collection",
				! user.sessionTokens.contains(token));
	}

	@Test
	public void testToJSONString() throws NoSuchUserException, SQLException, JSONException {
		User user = new UserTest(seedUsername);
		JSONObject obj = new JSONObject( user.toJSONString() );
		
		Set<String> actualKeys = new HashSet<String>(Arrays.asList( JSONObject.getNames(obj) ));
		Set<String> expectedKeys = new HashSet<String>(Arrays.asList( new String[] {"username", "name", "id"} ));
		
		assertEquals("User's serialized JSON did not contain the expected set of keys", actualKeys, expectedKeys);
		assertTrue("User's serialized JSON contains wrong username",
				obj.getString("username").equals(seedUsername));
		assertTrue("User's serialized JSON contains wrong name",
				obj.getString("name").equals(seedName));
		assertEquals("User's serialized JSON contains wrong id",
				obj.getInt("id"),
				getUserID(seedUsername));
	}
	
	private int getUserID(String username) throws SQLException {
		PreparedStatement statement = DBUtils.connect
				.prepareStatement("select id from users where username = ?");
		statement.setString(1, username);

		ResultSet res = statement.executeQuery();
		res.next();
		return res.getInt("id");
	}
	
	private String getSessionToken(String username) throws SQLException {
		PreparedStatement statement = DBUtils.connect
				.prepareStatement("select sessionToken from sessions where username = ?");
		statement.setString(1, username);

		ResultSet res = statement.executeQuery();
		res.next();
		return res.getString("sessionToken");
	}

}
