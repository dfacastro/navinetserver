use navinet;
-- select u.username as usr from users u, sessions s WHERE s.sessionToken = 'e39d56ce-cedb-4e32-a6f6-f68813e57bb1' AND u.username = s.username;

-- insert into users(username, password, name) values('sonia', '1234', 'Sonia Barbosa');

/*
select * from
users u, sessions s, (
select u.username from users u, sessions s WHERE s.sessionToken = 'e39d56ce-cedb-4e32-a6f6-f68813e57bb1' AND u.username = s.username
) as f
WHERE u.username = s.username AND s.username = f.username;
*/

select * from sessions where username = 'diogo';


DROP TRIGGER IF EXISTS generate_session_token;
CREATE TRIGGER generate_session_token
before insert on sessions
for each row
set new.sessionToken = UUID();


insert into sessions(username) values('diogo');

select * from sessions;

insert into sessions(username) values('lol');

